# AGP (Advanced Graphics Programming)

Repository for the Advanced Graphics Programming subject.
All the code in this repository is strictly for academic use.

## Instructions

1. Open Qt Creator
2. Open project AGP.pro
3. Configure a 64 bits compilation kit
4. Set the working directory to be the repository root (same directory in which this file is located)

## Rendering techniques:
* Deferred shading
* Normal and relief mapping
* Environment mapping with equirectangular images (bg. Image, environment lighting and reflections)
* Physically based rendering
* Bloom
* SSAO
* Water plane with reflection and refraction using du/dv textures
* Instancing

## Other features:
* C++ / OpenGL
* Qt widgets framework
* Camera navigation (mouse + wasd keys)
* Object selection via mouse picking
* Object manipulation (ert keys)
* Scene serialization (JSON)
* Model importing (Assimp)
